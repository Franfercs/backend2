import React from 'react'
import Nav from './components/Nav'
import Reserva from './components/Reserva'
import Lista from './components/Lista'
import Consumo from './components/Consumo'
import Detalle from './components/Detalle'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/reservar" component={Reserva} />
          <Route path="/listareservas" component={Lista} />
          <Route path="/consumo" component={Consumo} />
          <Route path="/detalle/:id" component={Detalle} />
        </Switch>
      </div>
    </Router>
  );
}

const Home = () => (
  <div>
    <h1>Backend</h1>
    <p>Integrantes: </p>
    <p>Francisco Candia</p>
    <p>Marcelo Molas</p>
  </div>
);

export default App;