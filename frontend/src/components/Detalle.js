import React,{useState,useEffect} from 'react';
import axios from 'axios';
import '../App.css';
import {Grid, Button, Card, CardContent, CardActions, Typography, TextField, 
    Dialog, DialogTitle, DialogContent, DialogActions, DialogContentText} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {Link, useParams} from 'react-router-dom'
import pdf from 'html-pdf';

export default function Detalle(){
    
    const [consumo, setConsumo] = useState([]);
    const [detalles, setDetalles] = useState([]);
    const [detalle, setDetalle] = useState([]);
    const [productos, setProductos] = useState([]);
    const [clientes, setClientes] = useState([]);
    const [cliente, setCliente] = useState([]);
    const [total, setTotal] = useState(0);
    const [open1, setOpen1] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [isValid, setIsValid] = useState(true);
    const [calcular, setCalcular] = useState(false);
    const [imprimir, setImprimir] = useState(false);
    const [fin, setFin] = useState(false);
    let { id } = useParams();

    const defaultProps = {
        options: clientes,
        getOptionLabel: (option) => option.nombre + ' ' + option.apellido,
    };

    const defaultProps2 = {
        options: productos,
        getOptionLabel: (option) => option.nombre
    }

    const handleChange = (field, value) => setCliente((prev) => ({ ...prev, [field]: value }));
    const handleDetalle = (field, value) => setDetalle((prev) => ({ ...prev, [field]: value }));
    const openRegistro = () => {
        setCliente({
          nombre: "",
          apellido: "",
          cedula: ""
        })
        setOpen1(true);
    };

    const openDetalle = () => {
        setDetalle({
            id_producto: "",
            Producto: "",
            cantidad: "",
        })
        setOpen2(true);
    };

    const closeRegistro = () => {
        setOpen1(false);
    };

    const closeDetalle = async() => {
        setOpen2(false);
    };

    const crearDetalle = () => {
        
        setDetalle((prev => ({
            ...prev,
            id_cabecera: consumo.id,
            id_producto: detalle.Producto.id
        })))
        setTotal((prev) => (prev + detalle.Producto.precio*detalle.cantidad))
        closeDetalle();
    }

    useEffect(async() => {
        await axios.get("http://localhost:9090/api/cliente/")
            .then(response => response.data).then(data => {
            setClientes(data);
            }).catch(console.log);
            setIsValid(true);
    }, [isValid]);

    useEffect(() => {
        let cont = 0;
        detalles.map((d) => {
            cont = cont + (d.Producto?.precio*d.cantidad)  
        })
        setTotal(cont)
    }, [calcular])

    useEffect(async() => {
        await axios.post('http://localhost:9090/api/detalle', detalle)
        .then(response => response.data).then(data => {setDetalles((prev)=>[...prev,data])}).
        catch(console.log)
    }, [detalle.id_cabecera])

    useEffect(async() => {
        await axios.get("http://localhost:9090/api/producto")
        .then(response => response.data).then(data => {
        setProductos(data);
        }).catch(console.log);
    }, []);

    useEffect(async() => {
        await axios.get("http://localhost:9090/api/consumo/ocupado/" + id)
        .then(response => response.data).then(data => {
        setConsumo(data[0]);
        }).catch(console.log);
    }, []);

    useEffect(async() => {
        await axios.get("http://localhost:9090/api/detalle/" + consumo?.id)
        .then(response => response.data).then(data => {
        setDetalles(data);
        }).catch(console.log);
        setCalcular(true);
    }, [consumo, detalles.length]);

    const handleRegistro = async() => {
        await axios.post('http://localhost:9090/api/cliente', cliente)
        .then(response => response.data).then(data => {
        setConsumo((prev) => ({ ...prev, Cliente: data}));
        }).catch(console.log);
        setIsValid(false);
        closeRegistro();
    }

    const updateConsumo = () => {
        
        setConsumo((prev => ({
                ...prev,
                id_cliente: consumo?.Cliente.id,
                id_mesa: id,
                estado: "Abierto",
                total: total,
            })))

        if(!consumo?.id){setConsumo((prev) => ({ ...prev, horafecha_creacion: new Date() }))}
    }

    useEffect(async() => {
        if(consumo.id){
            await axios.put('http://localhost:9090/api/consumo/' + consumo.id, consumo)
            .then(response => response.data).
            catch(err=> {console.log("Error al actualizar: " + err); console.log(consumo)}); 
        }else{
            
            await axios.post('http://localhost:9090/api/consumo', consumo)
            .then(response => response.data)
            .catch(console.log)
        }
    }, [consumo?.estado, consumo?.id_cliente])

    const cerrarConsumo = async() => {

        setConsumo(prev => ({
            ...prev,
            id_cliente: consumo.Cliente.id,
            id_mesa: id,
            estado: "Cerrado",
            total: total,
            horafecha_cierre: new Date() 
        }))

        setFin(true);

    }

    useEffect(async() => {
        await axios.put('http://localhost:9090/api/consumo/' + consumo.id, consumo)
        .then(response => response.data).then(() =>{
            if (consumo.estado == "Cerrado"){
            console.log(consumo.total) 
            axios({
                url: "http://localhost:9090/api/consumo/" + consumo.id + "/factura",
                method: 'GET',
                responseType: 'blob', // important
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'file.pdf');
                document.body.appendChild(link);
                link.click();
            }).catch(err=>console.log(err))
        }}).
        catch(console.log)
    }, [fin])

    useEffect(async() => {
        
    }
    , [consumo?.estado])

    return(
        <div>
            <h1>
                Mesa {id}
            </h1>
            <Grid container direction="column" alignItems="center" justify="space-between" >                
                <Card style={{minWidth: 1000, marginBottom: 10, border: "1px solid"}} >
                    <CardContent>
                        {consumo?.id ?
                            <Typography>
                                Estado: Ocupado
                                <br/>
                                Cliente: {consumo?.Cliente?.nombre + ' ' + consumo?.Cliente?.apellido}
                            </Typography>
                        :
                            <Typography>
                                Estado: Desocupado
                            </Typography>
                        }
                        <Autocomplete
                            {...defaultProps}
                            id="debug"
                            value = {consumo?.Cliente}
                            onChange = {(event,newValue) => {
                                setConsumo((prev) => ({ ...prev, Cliente: newValue }))
                            }}
                            autoComplete
                            includeInputInList
                            renderInput={(params) => <TextField {...params} label="Cliente" margin="normal" />}
                        />
                        <br/>
                        <Button onClick={openRegistro} variant="contained" color="primary">
                            Registrar Cliente
                        </Button>
                        <Typography>
                            <br/>Consumo Total: {total} Gs.
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" href="http://localhost:3000/consumo" color="primary">
                            Salir
                        </Button>
                        {consumo?.id ?
                            <div>
                            <Button variant="contained" onClick={cerrarConsumo} color="primary">
                                Cerrar Consumo
                            </Button>
                            <Button style={{margin: 10}} variant="contained" onClick={updateConsumo} color="primary">
                                Actualizar
                            </Button>
                            </div>
                        :
                            <div>
                            <Button variant="contained" onClick={updateConsumo} color="primary">
                            Crear
                            </Button>
                            </div>
                        }
                    </CardActions>
                </Card>
                    <Card style={{minWidth: 1000, border: "1px solid"}}>
                        <CardContent>
                            <Typography variant="h5">
                                Detalles
                            </Typography>
                            {detalles.map((d => {
                                return(
                                    <Card style={{minWidth: 1000, marginBottom: 10, border: "1px solid"}}>
                                        <CardContent>
                                            <Typography variant="h5">
                                                {d?.Producto?.nombre}<br/>
                                                <br/>
                                            </Typography>
                                            <Typography>
                                                Precio: {d?.Producto?.precio} Gs.<br/>
                                                Cantidad: {d?.cantidad}<br/>
                                                Total: {d?.Producto?.precio*d?.cantidad} Gs.<br/>
                                            </Typography>
                                        </CardContent>
                                    </Card>
                                )
                            }))}
                        </CardContent>
                        <CardActions>
                            {consumo?.id ?
                                <Button variant="contained" color="primary" onClick={openDetalle}>
                                    Nuevo
                                </Button>
                            :
                                <Typography>
                                    Debe crear una cabecera primero
                                </Typography>
                            }
                        </CardActions>
                    </Card>
            </Grid>
            <Dialog open={open1} onClose={closeRegistro} aria-labelledby="form-dialog-title1">
                <DialogTitle id="form-dialog-title1">Agregar Cliente</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Complete los campos para agregar un nuevo cliente.
                    </DialogContentText>
                    <TextField margin="dense" id="nombre" label="Nombre"
                    type="text" value = {cliente.nombre} 
                    onChange = {(event) => { handleChange("nombre",event.target.value);
                    }}
                    />
                    <TextField margin="dense" id="apellido" label="Apellido"
                    type="text" value = {cliente.apellido} 
                    onChange = {(event) => { handleChange("apellido",event.target.value);
                    }}
                    />
                    <TextField margin="dense" id="cedula" label="Cedula"
                    type="text" value = {cliente.cedula} 
                    onChange = {(event) => { handleChange("cedula",event.target.value);
                    }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeRegistro} color="primary">
                    Cancelar
                    </Button>
                    <Button onClick={() => {handleRegistro(cliente)}} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={open2} onClose={closeDetalle} aria-labelledby="form-dialog-title2">
                <DialogTitle id="form-dialog-title2">Agregar Detalle</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Complete los campos para agregar un nuevo detalle.
                    </DialogContentText>
                    <Autocomplete
                            {...defaultProps2}
                            id="debug"
                            value = {detalle?.Producto}
                            onChange = {(event,newValue) => {
                                setDetalle((prev) => ({ ...prev, Producto: newValue }))
                            }}
                            autoComplete
                            includeInputInList
                            renderInput={(params) => <TextField {...params} label="Producto" margin="normal" />}
                    />
                    <TextField margin="dense" id="cantidad" label="Cantidad"
                    type="number" value = {detalle.cantidad} 
                    onChange = {(event) => {handleDetalle("cantidad",event.target.value)}}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    />
                    {(detalle.Producto && detalle.cantidad) ? 
                        <Typography>
                            Precio: {detalle.Producto.precio*detalle.cantidad} Gs.
                        </Typography>
                    :
                        <Typography>
                            Precio: 0 Gs.
                        </Typography>
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDetalle} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={() => {crearDetalle(detalle)}} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}