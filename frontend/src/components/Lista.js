import React,{useState,useEffect} from 'react';
import Select from 'react-select';
import axios from 'axios';
import '../App.css';
import {Grid, makeStyles, Button, TextField, Card, Table, TableHead, TableBody, TableCell, TableRow, TableContainer} from '@material-ui/core';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
        spacing: 8,
    },
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    div: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }
    })

function Lista(){

    const [datos1,setDatos1] = useState([]);
    const [datos2,setDatos2] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        axios.get("http://localhost:9090/api/restaurante/")
            .then(response => response.data).then(data => {
            setDatos1(data);
            }).catch(console.log);
    }, []);
    
    useEffect(() => {
        axios.get("http://localhost:9090/api/cliente/")
            .then(response => response.data).then(data => {
            setDatos2(data);
            }).catch(console.log);
    }, []);

    const [reservas,setReserva] = useState([]);

    const [restaurante,setRestaurante] = useState(
        {
            id: "",
            nombre: ""
        }
    );

    const [date,setDate] = useState(new Date())

    const [cliente,setCliente] = useState(
        {
            label: "",
            id: ""
    });

    const handleClick = async () => {
        await axios.get('http://localhost:9090/api/reserva/' + restaurante.id + '/' + new Date(date.getTime() - (date.getTimezoneOffset() * 60000 )).toISOString().split("T")[0] + '/' + cliente.id)
        .then(response => response.data).then(data => setReserva(data)).catch(console.log);
    }

    return(
        <div>
        <h1>Lista de Reservas</h1>
        <Grid container justify="center" spacing={3}>
            <Grid item xs={6}>
                    <Select
                    name="restaurante"
                    options={datos1.map(d => ({label: d.nombre, value: d.id}))}
                    onChange = {(event) => setRestaurante({ id: event.value, nombre: event.label })
                    }
                    />
            </Grid>
            <Grid item xs={6}>   
                <DatePicker selected={date} dateFormat="yyyy-MM-dd" onChange={date => setDate(date)} />
            </Grid>
            <Grid item xs={6}>
                <Select
                name="cliente"
                options={datos2.map(d => ({label: d.nombre + ' ' + d.apellido, value: d.id}))}
                onChange = {(event) => setCliente({ label: event.label, id: event.value})}
            />
            </Grid>
            <Grid item xs={6}>
                <Button variant="contained" color="primary" onClick = {handleClick}>Buscar</Button>
            </Grid>
        </Grid>
        <TableContainer component={Card}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Cliente&nbsp;</TableCell>
                        <TableCell>Mesa&nbsp;</TableCell>
                        <TableCell>Hora de Inicio&nbsp;</TableCell>
                        <TableCell>Hora de Fin&nbsp;</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {reservas.map((reserva) => (
                        <TableRow key={reserva.id}>
                            <TableCell>{reserva.Cliente.nombre + ' ' + reserva.Cliente.apellido}</TableCell>
                            <TableCell>{reserva.Mesa.nombre}</TableCell>
                            <TableCell>{reserva.hora_inicio}</TableCell>
                            <TableCell>{reserva.hora_fin}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        </div>
    );
}

export default Lista;