import React,{useState,useEffect} from 'react';
import axios from 'axios';
import '../App.css';
import {Grid, Button, TextField, List, ListItem, ListItemText,
 ListItemIcon, ListSubheader, Collapse} from '@material-ui/core';
import Restaurante from './Restaurante';

function Consumo(){

    const [restaurantes, setRestaurantes] = useState([]);

    useEffect(() => {
        axios.get("http://localhost:9090/api/restaurante/")
            .then(response => response.data).then(data => {
            setRestaurantes(data);
            }).catch(console.log);
    }, []);

    return(
        <div>
            <h1>Restaurantes</h1>
            <List>
                { restaurantes.map((restaurante) => {
                    return(
                        <Restaurante restaurante={restaurante}/>
                )})}
            </List>
        </div>
    );
}

export default Consumo;