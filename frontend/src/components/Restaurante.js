import React,{useState,useEffect} from 'react';
import axios from 'axios';
import '../App.css';
import {List, ListItem, ListItemText, Collapse} from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import {Link} from 'react-router-dom';    

function Restaurante(props){

    const [mesas, setMesas] = useState([]);
    const [open, setOpen] = useState(true);

    useEffect(() => {
        axios.get("http://localhost:9090/api/mesa/restid/" + props.restaurante.id)
            .then(response => response.data).then(data => {
            setMesas(data);
            }).catch(console.log);
    }, []);

    const handleOpen = async() => {
        setOpen(!open);
    };

    return(
        <List>
            <ListItem button style={{backgroundColor: "#4850d1", color: "#FFFFFF"}} onClick={handleOpen}>
                <ListItemText primary={props.restaurante.nombre} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            { mesas ?
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    {mesas.map((mesa)=>{
                        return (
                            <Link to={"detalle/" + mesa.id} >
                                <ListItem button>
                                    <ListItemText primary={mesa.nombre} />
                                </ListItem>
                            </Link>
                        )
                    })}
                </List>
            </Collapse>
            :
            <Collapse in={open} timeout="auto" unmountOnExit>
                No hay mesas para mostrar.
            </Collapse>
            }
        </List>
    )
}

export default Restaurante;