import React,{useState,useEffect} from 'react';
import Select from 'react-select';
import axios from 'axios';
import '../App.css';
import {Grid, Button, TextField} from '@material-ui/core';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

function Reserva(){

    const horas = [12, 13, 14, 15, 16, 17, 18 , 19, 20, 21, 22, 23];
    const [datos,setDatos] = useState([])
    useEffect(() => {
        axios.get("http://localhost:9090/api/restaurante/")
            .then(response => response.data).then(data => {
            setDatos(data);
            }).catch(console.log);
    }, []);

    const [cliente,setCliente] = useState(
        {
          nombre: "",
          apellido: "",
          cedula: ""
    });
    
    const [cedula,setCedula] = useState("")
    const [reserva,setReserva] = useState(
        {
          restaurante_id: "",
          cliente_id: "",
          mesa_id: "",
          fecha: new Date(),
          hora_inicio: "",
          hora_fin: "",
          cantidad: ""
    });

    const [mesas, setMesas] = useState([])
    const [showCliente, setShowCliente] = useState(false)

    const handleClick = async () => {
        await axios.get('http://localhost:9090/api/reserva/' + reserva.restaurante_id + '/' + new Date(reserva.fecha.getTime() - (reserva.fecha.getTimezoneOffset() * 60000 )).toISOString().split("T")[0] + '/' + reserva.hora_inicio + '/' + reserva.hora_fin)
        .then(response => response.data).then(data => setMesas(data)).catch(console.log);
    }

    const handleClick2 = async() => {
        await axios.get('http://localhost:9090/api/cliente/ci/' + cedula)
        .then(response => response.data).then(data => {setCliente(data[0]); setReserva((prev) => ({ ...prev, cliente_id: data[0].id}))}).then(setShowCliente(true)).catch(() => (console.log, setShowCliente(false)));
    }

    const handleClick3 = async() => {
        setCliente((prev) => ({ ...prev, cedula: cedula}));
        await axios.post('http://localhost:9090/api/cliente', cliente)
        .then(response => response.data).then(data => setReserva((prev) => ({ ...prev, cliente_id: data.id }))).catch(console.log);
    }

    const handleClick4 = async() => {
        await axios.post('http://localhost:9090/api/reserva', reserva)
        .then(response => console.log(response)).catch(console.log);
    }

    return(
        <div>
        <h1>Reservar una mesa</h1>
        <Grid container justify="center" spacing={3}>
            <Grid item xs={6}>
                <Select
                defaultValue={datos[0]}
                name="restaurante"
                options={datos.map(d => ({label: d.nombre, value: d.id}))}
                onChange = {(event) => setReserva((prev) => ({ ...prev, restaurante_id: event.value }))
                }
                />
            </Grid>
            <Grid item xs={6}>   
                <DatePicker selected={reserva.fecha} dateFormat="yyyy-MM-dd" onChange={date => setReserva((prev) => ({ ...prev, fecha: date }))} />
            </Grid>
            <Grid item xs={6}>   
                    <Select 
                    defaultValue
                    name="hora_inicio"
                    options={horas.map(h => ({label: h, value: h}))}
                    onChange = {(event) => setReserva((prev) => ({ ...prev, hora_inicio: event.value }))
                    }
                    />
            </Grid>
            <Grid item xs={6}>   
                    <Select 
                    defaultValue
                    name="hora_fin"
                    options={horas.map(h => ({label: h, value: h}))}
                    onChange = {(event) => setReserva((prev) => ({ ...prev, hora_fin: event.value }))
                    }
                    />
            </Grid>
        </Grid>
        <Grid container justify="center" spacing ={3}>
            <Grid item xs={6}>
                <Button onClick={handleClick} variant="contained" color="primary">
                    Buscar
                </Button>
            </Grid>
        </Grid>
        <Grid container justify="center" spacing={3}>
            <Grid item xs={6}>
                <Select 
                    defaultValue
                    name="mesa"
                    options={mesas.map(m => ({label: m.nombre + ' Capacidad: ' + m.capacidad, value: m.id, value2: m.capacidad}))}
                    onChange = {(event) => setReserva((prev) => ({ ...prev, mesa_id: event.value, cantidad: event.value2}))
                    }
                />
            </Grid>
        </Grid>
        <Grid container justify="center" spacing={3}>
            <Grid item xs={6}>
                    <TextField
                    autoFocus
                    margin="dense"
                    id="cliente"
                    label="Cliente"
                    type="text"
                    fullWidth
                    InputLabelProps={{ shrink: true }}
                    onChange={(event) => setCedula(event.target.value)
                    }
            />
            </Grid>
        </Grid>
        <Grid container justify="center" spacing={3}>
            <Button onClick={handleClick2} variant="contained" color="primary">
                    Buscar
            </Button>
        </Grid>
            {showCliente ?
                <Grid container justify="center" direction="column" alignItems="center" spacing={3}>
                    <Grid item xs={6}>
                        <TextField
                        autoFocus
                        disabled
                        margin="dense"
                        id="nombre"
                        label="Nombre"
                        type="text"
                        fullWidth
                        value = {cliente.nombre}
                        InputLabelProps={{ shrink: true }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                        autoFocus
                        disabled
                        margin="dense"
                        id="apellido"
                        label="Apellido"
                        type="text"
                        value = {cliente.apellido}
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        />
                    </Grid>
                </Grid>
                :
                <Grid container justify="center" direction="column" alignItems="center" spacing={3}>
                    <Grid item xs={6}>
                        <TextField
                        autoFocus
                        margin="dense"
                        id="nombre2"
                        label="Nombre"
                        type="text"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        onChange={(event) => setCliente((prev) => ({cedula: prev.cedula, apellido: prev.apellido , nombre: event.target.value }))
                        }/>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                        autoFocus
                        margin="dense"
                        id="apellido2"
                        label="Apellido"
                        type="text"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        onChange={(event) => setCliente((prev) => ({cedula: prev.cedula, nombre: prev.nombre, apellido: event.target.value }))
                        }/>
                    </Grid>
                    <Grid item xs={6}>
                        <Button onClick={handleClick3} variant="contained" color="primary">
                            Registrar
                        </Button>
                    </Grid>
                </Grid>
            }
            <Grid container justify="center" direction="column" alignItems="center" spacing={3}>
                <Button onClick={handleClick4} variant="contained" color="primary">
                    Reservar
                </Button>
            </Grid>
        </div>
    );
}

export default Reserva;